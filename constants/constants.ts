export const STEP_SEARCHPARAM_KEY = 'step';

export const API_URL =
  process.env.NEXT_PUBLIC_API_URL ?? 'http://localhost:3000';
export const FORM_ID = process.env.NEXT_PUBLIC_FORM_ID ?? 'test-form';
export const CLIENT_NAME =
  process.env.NEXT_PUBLIC_CLIENT_NAME ?? 'example-user';

export const DEFAULT_PATHNAME = process.env.NEXT_PUBLIC_DEFAULT_PATHNAME ?? '/';
